const rightAnswerIndexes = [0, 2, 2];
let rightAnswersCount = 0;
let wrongAnswersCount = 0;
let noAnswer = 0;

document.querySelector('#resultbtn').addEventListener('click', () => {
    let answerNodes = document.querySelectorAll('.answers');

    //Find answer-nodes and values and compare the right question indexes
    for (let b = 0; b < answerNodes.length; b++) {
        let answers = answerNodes[b].querySelectorAll('.answer');

        // Check right answer
        let isAnsweredQuestion = false
        for (let i = 0; i < answers.length; i++) {
            if (answers[i].querySelector('input').checked == true) {
                rightAnswerIndexes[b] == i ? rightAnswersCount++ : wrongAnswersCount++;
                isAnsweredQuestion = true;
                break;
            }
        }
        if (isAnsweredQuestion == false) noAnswer++;
    }
    //Output
    alert(`Правильно:${rightAnswersCount} (${Math.round(rightAnswersCount / (rightAnswersCount + wrongAnswersCount + noAnswer) * 100)}%) 
    Неправильно:${wrongAnswersCount}
    Не отвечено:${noAnswer}`);

    //Reset counters
    rightAnswersCount = 0; wrongAnswersCount = 0; noAnswer = 0;
});