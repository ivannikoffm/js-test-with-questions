//#region  Global data and variables
const rightAnswersArray = [
    [0, 3],
    [0, 3, 4],
    [0, 2, 4]
];
let questionNodes = document.querySelector('#questions-chbx');

//#endregion

document.querySelector('#nextbtn').addEventListener('click', () => {
    for (let i = 0; i < questionNodes.children.length; i++) {
        // Find the actual visible element
        if (!questionNodes.children[i].classList.contains('hidden')) {

            //Compare checkboxes user checked and right answer indexes
            let checkResult = checkRightAnswer(questionNodes.children[i].querySelector('.answers-chbx'), rightAnswersArray[i]);
            if (checkResult == 1) rightAnswersCount++;
            else if (checkResult == 2) wrongAnswersCount++;
            else if (checkResult == 3) noAnswer++;

            // Toggle hidden and visible questions and show result
            if (i == questionNodes.children.length - 2) document.querySelector('#nextbtn').value = 'Показать результат';
            if (i == questionNodes.children.length - 1) { output(); resetAnswers(); }

            else questionNodes.children[i].nextElementSibling.classList.toggle('hidden');

            questionNodes.children[i].classList.toggle('hidden');
            return;
        }
    }
});

// Collect checked user elements and compare it with right answers. Returns Right, Wrong, No Answer
function checkRightAnswer(node, rightAnswers) {
    let checkedUserIndexes = [];
    for (let i = 0; i < node.children.length; i++) {
        if (node.children[i].querySelector('input').checked == true) {
            checkedUserIndexes.push(i);
        }
    }
    if (checkedUserIndexes.join() == rightAnswers.join()) return 1;
    else if (checkedUserIndexes.length == 0) return 3;
    else if (checkedUserIndexes.join() != rightAnswers.join()) return 2;

}

function output() {
    alert(`Правильно:${rightAnswersCount} (${Math.round(rightAnswersCount / (rightAnswersCount + wrongAnswersCount + noAnswer) * 100)}%) 
    Неправильно:${wrongAnswersCount}
    Не отвечено:${noAnswer}`);
}

// Reset all data and global variables to default values (after last question)
function resetAnswers() {
    rightAnswersCount = 0;
    wrongAnswersCount = 0;
    noAnswer = 0;
    document.querySelector('#nextbtn').value = 'Следующий вопрос';
    questionNodes.children[0].classList.toggle('hidden');
}