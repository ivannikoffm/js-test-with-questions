let pollAnswersArray = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0]
];
let pollQuestions = document.querySelectorAll('.poll-question');

document.querySelector('#poll-inputbtn').addEventListener('click', () => {
    for (let i = 0; i < pollQuestions.length; i++){
        let answers = pollQuestions[i].querySelectorAll('.poll-answer');  
        let inputValue = answers[answers.length - 1].querySelector('input').value;
        
        // If user adds custom answer
        if (inputValue.length != 0) {
            addNewAnswer(inputValue, answers, i);
            answers = pollQuestions[i].querySelectorAll('.poll-answer'); 
        }
        
        // Searching for checked answer and increase counter in array
        let isChecked = false;
        for (let a = 0; a < answers.length; a++){
            if (answers[a].querySelector('input').checked == true) {
                pollAnswersArray[i][a]++;
                isChecked = true;
            }
        }
        if (isChecked == false) continue;

        //Output and counting poll answers percentage
        for (let a = 0; a < answers.length-1; a++) {
            let r = answers[a].querySelector('span').innerText.length;
            let y = (answers[a].querySelector('span').innerText.length == 0);
            answers[a].querySelector('span').innerText = getPollPercent(pollAnswersArray[i], a);
        }
    }
});

function getPollPercent(answersArray, answerIndex) {
    let sumArray = 0;
    for (let el of answersArray)
        sumArray += el;
    return Math.round((answersArray[answerIndex] / sumArray) * 100) + '%';
}

// If user adds custom answer, release:
function addNewAnswer(inputValue, answers, i) {
    let li = createLI(inputValue + ' ', answers[i].querySelector('input').name); // Create new li-answer

    pollQuestions[i].querySelector('ul').lastElementChild.previousElementSibling.appendChild(li);  // Append the element to all-answers node
    answers[answers.length - 1].querySelector('input').value = ''; // Reset input value
    pollAnswersArray[i].push(0); // Push the poll-array with new element
}

// Creat new li-answer node using Users input data
function createLI(value, name) {
    let li = document.createElement('li');
    li.setAttribute('class', 'poll-answer');
    li.innerHTML = value;

    let span = document.createElement('span');
    let input = document.createElement('input');
    input.setAttribute('type', 'radio');
    input.setAttribute('name', name);
    input.checked = true;
    li.insertBefore(input, li.childNodes[0]);
    li.appendChild(span)
    return li;
}